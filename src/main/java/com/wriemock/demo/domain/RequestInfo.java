package com.wriemock.demo.domain;

import java.util.Map;

/*
 * create by shibs on 2020/7/15 0015
 */
public class RequestInfo {


    private String code;

    private int page;

    private int pageSize;



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}