package com.wriemock.demo.domain;

/*
 * create by shibs on 2020/7/16 0016
 */
public class RequestHeader {

    private String contentType;

    private String XCaKey;

    private String XCaSignature;

    private String XCaTimestamp;

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getXCaKey() {
        return XCaKey;
    }

    public void setXCaKey(String XCaKey) {
        this.XCaKey = XCaKey;
    }

    public String getXCaSignature() {
        return XCaSignature;
    }

    public void setXCaSignature(String XCaSignature) {
        this.XCaSignature = XCaSignature;
    }

    public String getXCaTimestamp() {
        return XCaTimestamp;
    }

    public void setXCaTimestamp(String XCaTimestamp) {
        this.XCaTimestamp = XCaTimestamp;
    }


}