package com.wriemock.demo.domain;

import java.util.List;
import java.util.Map;

/*
 * create by shibs on 2020/7/16 0016
 */
public class ResponseInfo {

    private String code;


    private List<Map> content;

    private String errorMessage;


    private String traceId;

    public ResponseInfo(){

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }


    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public List<Map> getContent() {
        return content;
    }

    public void setContent(List<Map> content) {
        this.content = content;
    }
}