package com.wriemock.demo.domain;

import java.util.Map;

/*
 * create by shibs on 2020/7/20 0020
 */
public class MessageRequest {
    private String business;
    private String event;
    private Map data;

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Map getData() {
        return data;
    }

    public void setData(Map data) {
        this.data = data;
    }
}