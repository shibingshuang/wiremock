package com.wriemock.demo.service;

import com.alibaba.fastjson.JSONObject;
import com.wriemock.demo.component.Constants;
import com.wriemock.demo.component.HttpPostUtil;
import com.wriemock.demo.domain.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


import java.io.*;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.*;

/*
 * create by shibs on 2020/7/15 0015
 */
@Service
public class RemoteService {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${remote1}")
    private String url1;

    @Value("${remote2}")
    private String url2;

    public String access(JSONObject paramsMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json;charset=UTF-8");
        headers.add("X-Ca-Key","203718395");
        headers.add("X-Ca-Signature","6q0A9rYw3f1IymGjG+SctKkFyxdlYFCS2FzmnHeLSMw=");
        headers.add("X-Ca-Timestamp",String.valueOf(new Date().getTime()));
        headers.add("traceId","traceId"+String.valueOf(new Date().getTime()));



        HttpEntity<JSONObject> request = new HttpEntity<>(paramsMap, headers);

        System.out.println(request.getHeaders());
        System.out.println(request.getBody());
        System.out.println(url1);
        ResponseEntity<JSONObject> response = restTemplate.postForEntity(url1, request , JSONObject.class );
        //ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST,request,String.class);
        System.out.println(response.getBody());
        String code = response.getBody().get("code").toString();

        return code;
    }


    public MessageResponse access2(RequestInfo req1, RequestInfo req2) {
        ResponseInfo res1 = getEvaluationDetail(req1);
        ResponseInfo res2 = getlistEvaluationItem(req2);

        MessageResponse response = new MessageResponse();

        if(res1 != null && res2 != null){
            response.setCode(res1.getCode());
            response.setErrorMessage(res1.getErrorMessage());
            response.setTraceId("traceId"+String.valueOf(new Date().getTime()));
            response.setContent(true);
        }
        return response;
    }


    public ResponseInfo getEvaluationDetail(RequestInfo req){
        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setContentType(Constants.JsonType);
        requestHeader.setXCaKey(Constants.AppKey);
        requestHeader.setXCaSignature(Constants.Signature1);
        requestHeader.setXCaTimestamp(String.valueOf(new Date().getTime()));
        ResponseInfo responseInfo = HttpPostUtil.getPost(req,requestHeader,url1);
        return responseInfo;
    }

    public ResponseInfo getlistEvaluationItem(RequestInfo req){
        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setContentType(Constants.JsonType);
        requestHeader.setXCaKey(Constants.AppKey);
        requestHeader.setXCaSignature(Constants.Signature2);
        requestHeader.setXCaTimestamp(String.valueOf(new Date().getTime()));
        ResponseInfo responseInfo = HttpPostUtil.getPost(req,requestHeader,url2);
        return responseInfo;
    }


}