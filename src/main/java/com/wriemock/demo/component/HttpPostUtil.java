package com.wriemock.demo.component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wriemock.demo.domain.RequestHeader;
import com.wriemock.demo.domain.RequestInfo;
import com.wriemock.demo.domain.ResponseInfo;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * create by shibs on 2020/7/16 0016
 */
@Component
public class HttpPostUtil {

    public static ResponseInfo getPost(RequestInfo requestInfo, RequestHeader header, String url){
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new StringEntity(JSONObject.toJSON(requestInfo).toString(),"UTF-8"));

        httpPost.addHeader("Content-Type",header.getContentType());
        httpPost.addHeader("X-Ca-Key",header.getXCaKey());
        httpPost.addHeader("X-Ca-Signature",header.getXCaSignature());
        httpPost.addHeader("X-Ca-Timestamp",header.getXCaTimestamp());

        HttpClient httpClient = HttpClients.createDefault();
        StringBuilder total = new StringBuilder();
        try {
            HttpResponse httpResponse = httpClient.execute(httpPost);
            if(httpResponse.getStatusLine().getStatusCode() == 200){
                InputStream ip = httpResponse.getEntity().getContent();
                BufferedReader rd = new BufferedReader(new InputStreamReader(ip));
                String line = "";
                while((line = rd.readLine()) != null){
                    total.append(line);
                }
                System.out.println(total.toString());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = JSONObject.parseObject(total.toString());
        ResponseInfo responseInfo = JSON.toJavaObject(jsonObject, ResponseInfo.class);

        return responseInfo;

    }


}