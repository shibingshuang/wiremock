package com.wriemock.demo.controller;

import com.alibaba.fastjson.JSON;
import com.wriemock.demo.domain.MessageRequest;
import com.wriemock.demo.domain.MessageResponse;
import com.wriemock.demo.domain.RequestInfo;
import com.wriemock.demo.domain.ResponseInfo;
import com.wriemock.demo.service.RemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

/*
 * create by shibs on 2020/7/16 0016
 */
@RestController
public class MessageController {
    @Autowired
    private RemoteService service;

    @CrossOrigin
    @ResponseBody
    @RequestMapping(value = "/api/message", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public MessageResponse getMessage(HttpServletRequest request) {
        StringBuilder total = new StringBuilder();
        MessageResponse response = null;
        try {
            InputStream inp = request.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(inp));
            String line = "";
            while((line = rd.readLine()) != null){
                total.append(line);
            }
            MessageRequest requestDomain = JSON.toJavaObject(JSON.parseObject(total.toString()),MessageRequest.class);


            String business = requestDomain.getBusiness();
            String event = requestDomain.getEvent();

            Map data = null;
            RequestInfo req1 = new RequestInfo();
            RequestInfo req2 = new RequestInfo();
            if("EVALUATION".equals(business) && "CREATE".equals(event)){
                data = requestDomain.getData();
                req1.setCode((String) data.get("code"));
                req2.setCode((String) data.get("code"));
                req2.setPage(1);
                req2.setPageSize(100);
                response = service.access2(req1,req2);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}