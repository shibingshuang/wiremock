package com.wriemock.demo.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/*
 * create by shibs on 2020/7/16 0016
 */
@RestController
public class TemplateController {

    @CrossOrigin
    @ResponseBody
    @RequestMapping(value = "/api/test")
    public JSONObject getMessage(HttpServletRequest request) {
        String contextPath = request.getContextPath();
        String url = request.getRequestURI();
        String path = url.substring(contextPath.length());
        String method = request.getMethod();
        String contentType = request.getContentType();
        System.out.println(contextPath+"*******"+url+"******"+path+"******"+method+"******"+contentType);

        JSONObject jsonObject = new JSONObject();

        return jsonObject;

    }
}