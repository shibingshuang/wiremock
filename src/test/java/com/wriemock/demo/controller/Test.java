package com.wriemock.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/*
 * create by shibs on 2020/7/16 0016
 */
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@AutoConfigureWireMock(stubs = "classpath:/mappings",port=8081)
public class Test {
    @Autowired
    private MockMvc mvc;


    @org.junit.Test
    public void bothServicesDoStuff() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("business", "EVALUATION");
        jsonObject.put("event", "CREATE");
        Map m = new HashMap();
        m.put("code","2007150000011");
        jsonObject.put("data", m);
        this.mvc.perform(MockMvcRequestBuilders.post("/api/message").contentType("application/json;charset=UTF-8").content(jsonObject.toJSONString())).andExpect(status().isOk())
                .andDo(print())         //打印出请求和相应的内容
                .andReturn().getResponse().getContentAsString();
    }
}